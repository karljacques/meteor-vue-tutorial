import Vue from 'vue';
import App from './App.vue';

import '../startup/accounts-config.js';

import VueMeteorTracker from 'vue-meteor-tracker';
Vue.use(VueMeteorTracker);

Meteor.startup(() => {
  new Vue({
    el: '#app',
    ...App,
  });
});